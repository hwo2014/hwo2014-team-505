import json
import socket
import sys


class NoobBot(object):

    previousAngle = 0
    pieceIndex = 0
    
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")
        
    def game_init(self, msg):
        race = msg['race']
        track = race['track']
        self.pieces = track['pieces']

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()
        
    def decide_to_switch_lane(self, pieceOne, pieceTwo):
        print("Race started")
        self.ping()

    def lap_finished(self, data):
        self.pieceIndex = 0
        self.ping()

    def on_car_positions(self, data):
        for car in data:
            id = car['id']
            name, color = id['name'], id['color']
            if name == self.name:
                
                piecePosition = car['piecePosition']
                pieceIndex = piecePosition['pieceIndex']
                if pieceIndex > self.pieceIndex:
                    self.pieceIndex = pieceIndex
                    
                    nbrOfPieces = len(self.pieces)
                    nextPiece = 0
                    nextNextPiece = 0
                    if pieceIndex+1 < nbrOfPieces:
                        nextPiece = pieceIndex+1
                    if pieceIndex+2 < nbrOfPieces:
                        nextNextPiece = pieceIndex+2

                    if 'switch' in self.pieces[nextPiece]:
                        if 'angle' in self.pieces[nextNextPiece]:
                            if self.pieces[nextNextPiece]['angle'] < 0:
                                self.msg("switchLane", "Right")
                            else:
                                self.msg("switchLane", "Left")
                            #return
                            
                angle = abs(car['angle'])
                if angle > self.previousAngle:
                    if angle < 1:
                        self.throttle(0.8)
                    elif angle < 2:
                        self.throttle(0.0)
                    elif angle < 3:
                        self.throttle(0.0)
                    elif angle > 3:
                        self.throttle(0.0)
                else:
                    self.throttle(1.0)
                self.previousAngle = angle
             

                            
                            
    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.game_init,
            'lapFinished': self.lap_finished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        print("UPDATED")
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
